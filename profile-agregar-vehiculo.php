<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Agregar vehículo - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationProfile.php') ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-12 text-center" id="barraLateral">
          <div id="profilePic">
            <img src="imgs/profile.jpg" class="img-fluid">
          </div>
            <img src="imgs/edit.svg" id="edit" width="30">

            <h2>Felipe Cairello</h2>

            <ul>
              <li><a href="profile-resumen.php"><img src="imgs/resumen.svg" width="40"><h3>RESUMEN</h3></a></li>
              <li><a href="profile-publicar-carga.php"><img src="imgs/publicarCarga.svg" width="40"><h3>PUBLICAR CARGA</h3></a></li>
              <li><a href="profile-buscar-carga.php"><img src="imgs/buscarCarga.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>BUSCAR CARGA</h3><span class="warning">2</span></a></li>
              <li><a href="JavaScript:void(0)" id="gestionar"><img src="imgs/gestionar.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>GESTIONAR</h3> <img src="imgs/arrow.svg" id="arrow" width="15"></a></li>
                <li class="submenu"><a href="profile-gestionar-cargas.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>CARGAS</h3></a></li>
                <li class="submenu"><a href="profile-gestionar-transportes.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>TRANSPORTES</h3></a></li>
              <li><a href="profile-editar-perfil.php"><img src="imgs/editarPerfil.svg" width="40"><h3>EDITAR PERFIL</h3></a></li>
              <li><a href="profile-facturacion.php"><img src="imgs/facturacion.svg" width="40"><h3>FACTURACIÓN</h3><span class="danger">1</span></a></li>
            </ul>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 text-center" id="contenido">
          <p id="breadcrumb"><a href="profile-resumen.php">Home</a> / <a href="profile-gestionar-transportes.php">Gestionar transportes</a> / Agregar vehículo</p>
          
          <div class="col-sm-12 contBlanco">
            <h3>Agregar vehículo</h3>

            <form onsubmit="return false">  

              <div class="row"> 
                <div class="col-lg-6 col-md-12 col-sm-12"> 
                    <label>Categoría</label>
                    <select class="form-control" name="">
                      <option> Liviano</option>
                      <option> Pesado</option>
                    </select>

                    <div class="col-md-1 col-sm-2">
                        <img src="imgs/liviano.svg" width="35">  
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <p class="aclaracion"> <strong>Livianos:</strong> (0-3500kg): autos, motos, bicicletas, camionetas y camiones livianos de 2 ejes</p>
                    </div>

                    <div class="clear"></div>

                    <div class="col-md-1 col-sm-2">
                        <img src="imgs/pesado.svg" width="40">  
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <p class="aclaracion"> <strong>Pesados:</strong> (+3500kg): transportes con 3 o más ejes</p>
                    </div>
                    
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12"> 
                    <label>Matrícula</label>
                    <input class="form-control" type="text" name="">
                </div>

                <div class="col-sm-12 text-center">
                    <button class="btn boton naranja" type="submit" style="margin: 0px auto; float: none"><p>Agregar vehículo</p></button>
                </div>

              </div>

            </form>

          </div>

        </div>

      </div>
    </div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/menu.js"></script>

  </body>

</html>
