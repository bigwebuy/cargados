<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Buscar carga - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationProfile.php') ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-12 text-center" id="barraLateral">
          <div id="profilePic">
            <img src="imgs/profile.jpg" class="img-fluid">
          </div>
            <img src="imgs/edit.svg" id="edit" width="30">

            <h2>Felipe Cairello</h2>

            <ul>
              <li><a href="profile-resumen.php"><img src="imgs/resumen.svg" width="40"><h3>RESUMEN</h3></a></li>
              <li><a href="profile-publicar-carga.php"><img src="imgs/publicarCarga.svg" width="40"><h3>PUBLICAR CARGA</h3></a></li>
              <li><a href="profile-buscar-carga.php" id="active"><img src="imgs/buscarCarga.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>BUSCAR CARGA</h3><span class="warning">2</span></a></li>
              <li><a href="JavaScript:void(0)" id="gestionar"><img src="imgs/gestionar.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>GESTIONAR</h3> <img src="imgs/arrow.svg" id="arrow" width="15"></a></li>
                <li class="submenu"><a href="profile-gestionar-cargas.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>CARGAS</h3></a></li>
                <li class="submenu"><a href="profile-gestionar-transportes.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>TRANSPORTES</h3></a></li>
              <li><a href="profile-editar-perfil.php"><img src="imgs/editarPerfil.svg" width="40"><h3>EDITAR PERFIL</h3></a></li>
              <li><a href="profile-facturacion.php"><img src="imgs/facturacion.svg" width="40"><h3>FACTURACIÓN</h3><span class="danger">1</span></a></li>
            </ul>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 text-center" id="contenido">
          <p id="breadcrumb"><a href="profile-resumen.php">Home</a> / Publicar carga</p>
          
          <div class="col-sm-12 contBlanco">
            <h3>Buscar carga</h3>

            <form onsubmit="return false">  
              <div class="row"> 
                <div class="col-sm-12"> 
                    <label>Vehículo</label>
                    <select class="form-control" name="" id="selectVehiculo">
                      <option> SFP 448</option>
                      <option> FGR 713</option>
                    </select>
                    <p id="agregaUnVehiculo">ó <a href="profile-agregar-vehiculo.php">Agrega un vehículo</a></p>
                </div>

              </div> 

              <div class="row"> 
                <div class="col-sm-6"> 
                    <label>Origen</label>
                    <input class="form-control location" type="text" name="">
                </div>

                <div class="col-sm-6"> 
                    <label>Destino <span>(opcional)</span></label>
                    <input class="form-control location" type="text" name="">
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12"> 
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3272.168226885838!2d-56.130524185138306!3d-34.902227580383226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81c45266fd21%3A0x76a91f3f93a29508!2sBigWeb+Uruguay!5e0!3m2!1ses-419!2suy!4v1527631654141" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>

              <div class="row text-center">
                  <button class="form-control" type="submit" style="margin:0 auto;" id="btnBuscar">Buscar cargas</button><br> 
              </div>

              <?php for($i=0; $i<10; $i++){ ?>
              <div class="resultado"> 
                <div class="col-lg-1 col-sm-2 col-xs-12"> 
                    <img src="imgs/publicarCarga.svg" width="40">
                </div>

                <div class="col-lg-3 col-sm-10 col-xs-12"> 
                    <label>Origen</label>
                    <p>Roberto Riverós 1281, Montevideo</p>
                </div>

                <div class="col-lg-3 offset-lg-0 offset-sm-2 col-sm-10 col-xs-12"> 
                    <label>Destino</label>
                    <p>Martín C Martínez 1617, Montevideo</p>
                </div>

                <div class="col-lg-2 offset-lg-0 offset-sm-2 col-sm-10 col-xs-12"> 
                    <label>Precio</label>
                    <h3 id="precio">$120</h3>
                </div>

                <div class="col-lg-3 offset-lg-0 offset-sm-2 col-sm-10 col-xs-12"> 
                    <label>Felipe Cairello (15)</label>
                    <div class="clear"></div>
                    <a href="profile-concretar-viaje.php" class="form-control naranja">Concretar viaje</a>
                </div>

                <div class="clear"></div>
              </div> 
              <?php } ?>

              <a class="verMas" href="JavaScript:void(0)"><button class="btn boton blanco"><p>Ver más</p></button></a>


            </form>

          </div>

        </div>

      </div>
    </div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/menu.js"></script>

    <script type="text/javascript">
      $(function () {
        $('.datetimepicker').datetimepicker();
      });


      $("#btnBuscar").click(function() {
        $(".resultado").show();
        $(".verMas").show();
      });

    </script>

    <style>  
    .resultado{
      display: none;
    }
    </style>


  </body>

</html>
