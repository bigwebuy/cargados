    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm" id="logoFooter"><br>
            <a href="index.php"><img src="imgs/logo.svg" width="48" style="float: left; margin:5px 10px 0 0;"><h2 class="logoCargados" style="padding-left: 58px;">Cargados<span>.uy</span></h2></a><br>
          </div>

          <div class="col-sm text-center"><br>
             <a href="mailto:info@cargados.uy"><h2 class="logoCargados"><span>info@</span>Cargados<span>.uy</span></h2></a><br>
          </div>

          <div class="col-sm text-center">
            <ul id="redes">
              <li><a href="#"><img src="imgs/twitter.svg"></a></li>
              <li><a href="#"><img src="imgs/facebook.svg"></a></li>
              <li><a href="#"><img src="imgs/instagram.svg"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <div id="copyright" class="text-center">
      <div class="container">
        <div class="col-sm-4">
          <p><a href="#">Términos y condiciones</a></p>
        </div>

        <div class="col-sm-4">
          <p><script type="text/javascript">
              copyright=new Date();
              update=copyright.getFullYear();
              document.write("Copyright © "+ update + " | cargados.uy");
              </script></p>
        </div>

        <div class="col-sm-4">
          <p>Diseño web: <a href="https://www.bigweb.com.uy/" title="Diseño web BigWeb" target="_blank">BigWeb</a></p>
        </div>

        <div class="clear"></div>
      </div>
    </div>
