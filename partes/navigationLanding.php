    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="imgs/logo.svg" width="66" style="float: left; margin:2px 10px 0 0; "><h2 class="logoCargados" style="padding-left: 58px;">Cargados<span>.uy</span></h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div style="width: 100%; text-align: center;">
          <h2 class="logoCargados" id="porQue"><span>¿Por qué elegir</span> Cargados<span>.uy?</span></h2>
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="login.php"><button class="btn boton conImg naranja"><p><img src="imgs/loginIcon.svg" width="22" style="float: left;"> Ingresar</p></button></a>
            </li>

            <li class="nav-item active">
              <a class="nav-link" href="registro.php"><button class="btn boton conImg blanco"><p><img src="imgs/registerIcon.svg" width="22" style="float: left;"> Registrarme</p></button></a>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>