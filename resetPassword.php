<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reestablecer contraseña - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationLanding.php') ?>

      <div class="fullscreen-bg">

          <video loop muted autoplay id="video_background"  media="screen and (min-width: 768px)"> 
            <source src="imgs/video.webm" type="video/webm"> 
            <source src="imgs/video.mp4" type="video/mp4"> 
            <source src="imgs/video.ogg" type="video/ogg"> 
          </video>  
      </div>
      <div class="container">

        <div class="col-lg-4 offset-lg-4 col-md-8 offset-md-2 col-sm-10 offset-sm-1" style="padding: 7% 0">
          <div class="block remove-bottom text-center gris contBla">
            <img src="imgs/key.svg" width="90">
            <h2>Reestablecer contraseña</h2>

            <form>
              <div class="col-sm-10 offset-sm-1"> 
                <label>Contraseña</label>
                <input class="form-control" type="password" name="">
              </div>

              <div class="col-sm-10 offset-sm-1"> 
                <label>Repetir contraseña</label>
                <input class="form-control" type="password" name="">
              </div>

              <div class="col-sm-10 offset-sm-1 text-center"> 
                <button class="btn boton blanco" type="submit"><p><img src="imgs/loginIcon.svg" width="22" style="float: left;"> Reestablecer contraseña</p></button>
              </div>

            </form>

            <div class="clear"></div>

          </div><!-- Video -->
        </div>
      </div>

      <div class="clear"></div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript">
      $("#porQue").click(function() {
        window.location.replace("index.php#beneficios")
      });
    </script>

  </body>

</html>
