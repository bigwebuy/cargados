<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registrarse - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationLanding.php') ?>

      <div class="fullscreen-bg">

          <video loop muted autoplay id="video_background"  media="screen and (min-width: 768px)"> 
            <source src="imgs/video.webm" type="video/webm"> 
            <source src="imgs/video.mp4" type="video/mp4"> 
            <source src="imgs/video.ogg" type="video/ogg"> 
          </video>  
      </div>
      <div class="container">

        <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
          <div class="block remove-bottom text-center gris contBla">
            <img src="imgs/registro.svg" width="90">
            <h2>Registro de Usuarios</h2>
            <div class="col-md-10 offset-md-1 col-sm-12">
              <form>
                <div class="col-md-6 col-sm-12"> 
                  <label>Nombre</label>
                  <input class="form-control" type="text" name="">
                  <div class="alerta">Por favor, ingresa tu nombre</div>
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Apellido</label>
                  <input class="form-control" type="text" name="">
                  <div class="alerta">Por favor, ingresa tu apellido</div>
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Celular</label>
                  <input class="form-control" type="text" name="">
                  <div class="alerta">Por favor, ingresa tu celular</div>
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Email</label>
                  <input class="form-control" type="email" name="">
                  <div class="alerta">Por favor, ingresa tu email</div>
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Rut <span>(opcional)</span></label>
                  <input class="form-control" type="text" name="">
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Razón social <span>(opcional)</span></label>
                  <input class="form-control" type="text" name="">
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Contraseña</label>
                  <input class="form-control" type="password" name="">
                  <div class="alerta">Por favor, escribe tu contraseña</div>
                </div>

                <div class="col-md-6 col-sm-12"> 
                  <label>Repetir contraseña</label>
                  <input class="form-control" type="password" name="">
                  <div class="alerta">Tus contraseñas no coinciden</div>
                </div>

                <div class="col-sm-10 offset-sm-1 text-center"> 
                  <button class="btn boton blanco" type="submit"><p><img src="imgs/loginIcon.svg" width="22" style="float: left;"> Ingresar</p></button>
                </div>

                <div class="col-sm-10 offset-sm-1 text-center" id="vinculo"> 
                  <p>¿Ya eres miembro? <a href="login.php">Inicia sesión</a></p>
                </div>

              </form>
            </div>

            <div class="clear"></div>

          </div><!-- Video -->
        </div>
      </div>

      <div class="clear"></div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript">
      $("#porQue").click(function() {
        window.location.replace("index.php#beneficios")
      });
    </script>

  </body>

</html>
