<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resumen - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationProfile.php') ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-12 text-center" id="barraLateral">
          <div id="profilePic">
            <img src="imgs/profile.jpg" class="img-fluid">
          </div>
            <img src="imgs/edit.svg" id="edit" width="30">

            <h2>Felipe Cairello</h2>

            <ul>
              <li><a href="profile-resumen.php" id="active"><img src="imgs/resumen.svg" width="40"><h3>RESUMEN</h3></a></li>
              <li><a href="profile-publicar-carga.php"><img src="imgs/publicarCarga.svg" width="40"><h3>PUBLICAR CARGA</h3></a></li>
              <li><a href="profile-buscar-carga.php"><img src="imgs/buscarCarga.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>BUSCAR CARGA</h3><span class="warning">2</span></a></li>
              <li><a href="JavaScript:void(0)" id="gestionar"><img src="imgs/gestionar.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>GESTIONAR</h3> <img src="imgs/arrow.svg" id="arrow" width="15"></a></li>
                <li class="submenu"><a href="profile-gestionar-cargas.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>CARGAS</h3></a></li>
                <li class="submenu"><a href="profile-gestionar-transportes.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>TRANSPORTES</h3></a></li>
              <li><a href="profile-editar-perfil.php"><img src="imgs/editarPerfil.svg" width="40"><h3>EDITAR PERFIL</h3></a></li>
              <li><a href="profile-facturacion.php"><img src="imgs/facturacion.svg" width="40"><h3>FACTURACIÓN</h3><span class="danger">1</span></a></li>
            </ul>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 text-center" id="contenido">
          <h2>Bienvenido a Cargados, Felipe</h2>
          
          <div class="col-sm-12 contBlanco">
            <h3>Mis cargas</h3>

            <table>
              <thead>
                <tr>
                  <th>Peso</th>
                  <th class="hideMe">Origen</th>
                  <th class="hideMe">Destino</th>
                  <th>Fecha</th>
                  <th>Hora</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php for($i=0; $i<3; $i++){ ?>
                <tr>
                  <td>15kg</td>  
                  <td class="hideMe">Roberto Riverós 1281, Montevideo, 11300</td>
                  <td class="hideMe">Martín C Martínez 1617, Montevideo, 11100</td>
                  <td>24 May</td>
                  <td>14:00</td>
                  <td class="tdEdit"><a href="#"><img src="imgs/editarPerfil.svg" width="30"></a></td>
                  <td class="tdTrash"><a href="#"><img src="imgs/trash.svg" width="30"></a></td>
                </tr>
                <?php } ?>
              </tbody>

            </table>
          </div>

          <div class="col-md-6 col-sm-12 colLeft">
            <div class=" contBlanco">
              <h3>Mis vehículos</h3>

              <table>
                <thead>
                  <tr>
                    <th class="hideMe">Categoría</th>
                    <th>Matrícula</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>

                <tbody>

                  <tr>
                    <td class="hideMe"><img src="imgs/liviano.svg" width="35" class="vehiculo"> Liviano</td>  
                    <td>SFP 488</td>
                    <td class="tdEdit"><a href="#"><img src="imgs/editarPerfil.svg" width="30"></a></td>
                    <td class="tdTrash"><a href="#"><img src="imgs/trash.svg" width="30"></a></td>
                  </tr>

                  <tr>
                    <td class="hideMe"><img src="imgs/pesado.svg" width="40" class="vehiculo"> Pesado</td>  
                    <td>FGR 713</td>
                    <td class="tdEdit"><a href="#"><img src="imgs/editarPerfil.svg" width="30"></a></td>
                    <td class="tdTrash"><a href="#"><img src="imgs/trash.svg" width="30"></a></td>
                  </tr>

                </tbody>

              </table>

              <a href="profile-agregar-vehiculo.php"><button class="btn boton blanco" style="margin-top: 15px"><p>Agregar vehículo</p></button></a>
            </div>
          </div>

          <div class="col-md-6 col-sm-12 colRight">
            <div class="contBlanco">
              <h3>Facturación</h3>

              <table>
                <thead>
                  <tr>
                    <th class="hideMe">Fecha</th>
                    <th class="hideMe">Concepto</th>
                    <th>Monto</th>
                    <th>Estado</th>
                  </tr>
                </thead>

                <tbody>

                  <tr>
                    <td class="hideMe">22 May</td>  
                    <td class="hideMe">Comisión por viaje</td>
                    <td>$30</td>
                    <td><a href="#" class="noPago">No pago</a></td>
                  </tr>

                  <tr>
                    <td class="hideMe">21 May</td>  
                    <td class="hideMe">Comisión por viaje</td>
                    <td>$30</td>
                    <td><a href="#">Pago</a></td>
                  </tr>

                  <tr>
                    <td class="hideMe">21 May</td>  
                    <td class="hideMe">Comisión por viaje</td>
                    <td>$30</td>
                    <td><a href="#">Pago</a></td>
                  </tr>

                </tbody>

              </table>

            </div>
          </div>
        </div>

      </div>
    </div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/menu.js"></script>

  </body>

</html>
