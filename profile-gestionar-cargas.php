<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gestionar cargas - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationProfile.php') ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-12 text-center" id="barraLateral">
          <div id="profilePic">
            <img src="imgs/profile.jpg" class="img-fluid">
          </div>
            <img src="imgs/edit.svg" id="edit" width="30">

            <h2>Felipe Cairello</h2>

            <ul>
              <li><a href="profile-resumen.php"><img src="imgs/resumen.svg" width="40"><h3>RESUMEN</h3></a></li>
              <li><a href="profile-publicar-carga.php"><img src="imgs/publicarCarga.svg" width="40"><h3>PUBLICAR CARGA</h3></a></li>
              <li><a href="profile-buscar-carga.php"><img src="imgs/buscarCarga.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>BUSCAR CARGA</h3><span class="warning">2</span></a></li>
              <li><a href="JavaScript:void(0)" id="gestionar"><img src="imgs/gestionar.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>GESTIONAR</h3> <img src="imgs/arrow.svg" id="arrow" width="15"></a></li>
                <li class="submenu"><a href="profile-gestionar-cargas.php" id="active"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>CARGAS</h3></a></li>
                <li class="submenu"><a href="profile-gestionar-transportes.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>TRANSPORTES</h3></a></li>
              <li><a href="profile-editar-perfil.php"><img src="imgs/editarPerfil.svg" width="40"><h3>EDITAR PERFIL</h3></a></li>
              <li><a href="profile-facturacion.php"><img src="imgs/facturacion.svg" width="40"><h3>FACTURACIÓN</h3><span class="danger">1</span></a></li>
            </ul>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 text-center" id="contenido">
          <p id="breadcrumb"><a href="profile-resumen.php">Home</a> / Gestionar cargas</p>
          
          <div class="col-sm-12 contBlanco">
            
            <div class="col-xs-12 text-center borderBottom">
              <a href="profile-publicar-carga.php"><button class="btn boton naranja" style="margin: 0px auto"><p>Agregar carga</p></button></a>
            </div>


            <h3>Mis cargas</h3>

            <form>  

            <?php for($i=0; $i<3; $i++){ ?>
              <div class="resultado"> 
                <div class="col-lg-1 col-sm-2 col-xs-12"> 
                    <img src="imgs/publicarCarga.svg" width="40">
                </div>

                <div class="col-lg-3 col-sm-10 col-xs-12"> 
                    <label>Origen</label>
                    <p>Roberto Riverós 1281, Montevideo</p>
                </div>

                <div class="col-lg-3 offset-lg-0 offset-sm-2 col-sm-10 col-xs-12"> 
                    <label>Destino</label>
                    <p>Martín C Martínez 1617, Montevideo</p>
                </div>

                <div class="col-lg-2 offset-lg-0 offset-sm-2 col-sm-10 "> 
                    <label>Precio</label>
                    <h3 id="precio">$120</h3>
                </div>

                <div class="col-lg-3 offset-lg-0 offset-sm-2 col-6" style="padding-bottom: 20px"> 
                      <div class="col-6">
                        <a href="#"><img src="imgs/editarPerfil.svg" width="30"></a>
                      </div>

                      <div class="col-6">
                        <a href="#"><img src="imgs/trash.svg" width="30"></a>
                      </div>

                </div>

                <div class="clear"></div>
              </div> 
            <?php } ?>
              
            </form>

            <a href="JavaScript:void(0)"><button class="btn boton blanco"><p>Ver más</p></button></a>

          </div>

        </div>

      </div>
    </div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="js/menu.js"></script> -->


    <style>
      .submenu{
        display: block;
      }

      #arrow{
        display: none;
      }
    </style>


  </body>

</html>
