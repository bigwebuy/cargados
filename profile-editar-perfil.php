<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Editar perfil - Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationProfile.php') ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-12 text-center" id="barraLateral">
          <div id="profilePic">
            <img src="imgs/profile.jpg" class="img-fluid">
          </div>
            <img src="imgs/edit.svg" id="edit" width="30">

            <h2>Felipe Cairello</h2>

            <ul>
              <li><a href="profile-resumen.php"><img src="imgs/resumen.svg" width="40"><h3>RESUMEN</h3></a></li>
              <li><a href="profile-publicar-carga.php"><img src="imgs/publicarCarga.svg" width="40"><h3>PUBLICAR CARGA</h3></a></li>
              <li><a href="profile-buscar-carga.php"><img src="imgs/buscarCarga.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>BUSCAR CARGA</h3><span class="warning">2</span></a></li>
              <li><a href="JavaScript:void(0)" id="gestionar"><img src="imgs/gestionar.svg" width="54" style="position: relative; right:10px; margin-right: -12px"><h3>GESTIONAR</h3> <img src="imgs/arrow.svg" id="arrow" width="15"></a></li>
                <li class="submenu"><a href="profile-gestionar-cargas.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>CARGAS</h3></a></li>
                <li class="submenu"><a href="profile-gestionar-transportes.php"><img src="imgs/arrowRight.svg" class="arrowRight" width="15"><h3>TRANSPORTES</h3></a></li>
              <li><a href="profile-editar-perfil.php" id="active"><img src="imgs/editarPerfil.svg" width="40"><h3>EDITAR PERFIL</h3></a></li>
              <li><a href="profile-facturacion.php"><img src="imgs/facturacion.svg" width="40"><h3>FACTURACIÓN</h3><span class="danger">1</span></a></li>
            </ul>

        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 text-center" id="contenido">
          <p id="breadcrumb"><a href="profile-resumen.php">Home</a> / Editar perfil</p>
          
          <div class="col-sm-12 contBlanco">
            <h3>Mis datos</h3>

            <form>  
              <div class="row"> 
                <div class="col-lg-4 col-sm-6"> 
                    <label>Nombre</label>
                    <input class="form-control" type="text" name="">
                </div>

                <div class="col-lg-4 col-sm-6"> 
                    <label>Apellido</label>
                    <input class="form-control" type="text" name="">
                </div>

                <div class="col-lg-4 col-sm-6"> 
                    <label>Celular</label>
                    <input class="form-control" type="text" name="">
                </div>

                <div class="col-lg-4 col-sm-6"> 
                    <label>Email</label>
                    <input class="form-control" type="email" name="">
                </div>

                <div class="col-lg-4 col-sm-6"> 
                    <label>RUT <span>(opcional)</span></label>
                    <input class="form-control" type="text" name="">
                </div>

                <div class="col-lg-4 col-sm-6"> 
                    <label>Razón Social <span>(opcional)</span></label>
                    <input class="form-control" type="text" name="">
                </div>
              </div> 

              

              <div class="row"> 
               <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-6 offset-sm-3"> 
                    <label>Foto de perfil</label>
                    <div class="clear"></div>
                    <div id="profilePic" style="margin: 5px auto; display: block!important;">
                      <img src="imgs/profile.jpg" class="img-fluid">
                    </div>
                    <img src="imgs/edit.svg" id="editP" width="30">

                </div>
              </div> 

              <div class="col-sm-12 text-center">
                  <button class="btn boton naranja" type="submit" style="margin: 0px auto; float: none"><p>Guardar cambios</p></button>
              </div>

            </form>

          </div>

        </div>

      </div>
    </div>

   <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/menu.js"></script>

    <script type="text/javascript">
      $(function () {
        $('.datetimepicker').datetimepicker();
      });
    </script>

  </body>

</html>
