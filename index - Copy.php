<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cargados.uy - Transporte de cargas</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/estilos.css" rel="stylesheet">

  </head>

  <body>

    <?php include('partes/navigationLanding.php') ?>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block h-100" src="imgs/slider.jpg" alt="First slide">
          <div class="carousel-caption d-md-block">
            <h2>¿VAS A VIAJAR, Y TENÉS ESPACIO PARA LLEVAR ALGO?</h2>
            <h3><img class="pinA" src="imgs/A.svg" width="50"><img class="pinB" src="imgs/B.svg" width="50"><div class="clear"></div></h3>

            <p>Encontrá las cargas publicadas que mejor se adapten a tu viaje y generá un ingreso extra con unos pocos clicks.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block h-100" src="imgs/slider.jpg" alt="First slide">
          <div class="carousel-caption d-md-block">
            <h2>TE AYUDAMOS A SOLUCIONAR EL ENVIO DE TU CARGA</h2>
            <h3><img class="pinA" src="imgs/A.svg" width="50"><img class="pinB" src="imgs/B.svg" width="50"><div class="clear"></div></h3>

            <p>Sólo debes publicar lo que querés llevar y a donde, desde el lugar que te encuentres, alguien de la comunidad de Cargados te lo llevará.</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block h-100" src="imgs/slider.jpg" alt="First slide">
          <div class="carousel-caption d-md-block">
            <h2>¿SOS FLETERO Y A VECES ESTAS CON EL VEHÍCULO PARADO?</h2>
            <h3><img class="pinA" src="imgs/A.svg" width="50"><img class="pinB" src="imgs/B.svg" width="50"><div class="clear"></div></h3>

            <p>En Cargados.uy encontrarás cargas de las más variadas y en cualquier momento del día. </p>
          </div>
        </div>

      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
      </a>
    </div>


    <div class="cta"> 
      <div class="container text-center">
        <div id="contenedorCta">
          <h2 class="logoCargados" style="float: left;"><span>Formá parte de la comunidad de</span> Cargados<span>.uy</span></h2>
          <a href="registro.php" style="float: right;" id="btnRegistro"><button class="btn boton blanco"><p><img src="imgs/registerIcon.svg" width="22" style="float: left;"> Registrarme</p></button></a>
        </div>
        <div class="clear"></div>
      </div>
    </div>

    <div id="video">
      <div class="text-center col-lg-8 offset-lg-2 col-md-12">
        <div id="contVideo">
          <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/biyCzjrZi1Y?ecver=2" width="640" height="360" frameborder="0" allow="autoplay; encrypted-media" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>  
        </div>
      </div>
      <div class="clear"></div>
    </div>

    <div id="beneficios">
      <div class="container">

        <div class="col-md-4 col-sm-6 text-center cajita">
          <img src="imgs/colaborativa.svg" width="150">
          <h3>ES CÓMODO</h3>
          <p>Publicá la carga que quieras, en el momento que quieras.</p>
        </div>

        <div class="col-md-4 col-sm-6 text-center cajita">
          <img src="imgs/camioneta.svg" width="150">
          <h3>ES SENCILLO</h3>
          <p>Podés llevar varias cargas de un mismo usuario en un solo viaje.</p>
        </div>

        <div class="col-md-4 col-sm-6 text-center cajita">
          <img src="imgs/candado.svg" width="150">
          <h3>ES CONFIABLE</h3>
          <p>Quien publica la carga y quien la transporta están debidamente registrados y forman parte de la comunidad.</p>
        </div>

        <div class="col-md-4 col-sm-6 text-center cajita">
          <img src="imgs/manos.svg" width="150">
          <h3>ES PRÁCTICO</h3>
          <p>Resolvés con unos pocos clicks desde donde estés, ahorrando tiempo y dinero</p>
        </div>

        <div class="col-md-4 col-offset-md-4 col-sm-6 col-offset-sm-3 text-center cajita" style="margin-bottom: 20px">
          <img src="imgs/billete.svg" width="150">
          <h3>ES RENTABLE</h3>
          <p>Optimizás el espacio de tu vehículo y ganás un dinero extra.</p>
        </div>


        <div class="clear"></div>

        <div class="text-center">
          <a href="registro.php""><button class="btn boton blanco"><p><img src="imgs/registerIcon.svg" width="22" style="float: left;"> Registrarme</p></button></a>
        </div>

      </div>
    </div>

    <div> 
        <h2 class="logoCargados" style="text-align: center; margin:40px auto 10px; color:#333333"><span>¿Cómo funciona</span> Cargados<span>.uy</span>?</h2>
        <div class="container steps text-center">

            <div class="col-md-3 col-sm-6 text-center">
              <h2>1</h2>
              <h3>Regístrate</h3>
              <p>Completá tus datos y ya estás listo para formar parte de la comunidad Cargados.uy</p>
            </div>

            <div class="col-md-3 col-sm-6 text-center">
              <h2>2</h2>
              <h3>Publicá tu carga</h3>
              <p>Estés donde estés, publicá que querés llevar, de donde a donde.</p>
            </div>

            <div class="col-md-3 col-sm-6 text-center">
              <h2>3</h2>
              <h3>Buscá carga para llevar</h3>
              <p>Si querés llevar carga y estás registrado, sólo tienes que encontrar la más conveniente</p>
            </div>

            <div class="col-md-3 col-sm-6 text-center">
              <h2>4</h2>
              <h3>Concretá tu viaje</h3>
              <p>Ponte en contacto con la otra parte y coordina lo que te queda mejor</p>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    


    <?php include('partes/footer.php') ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script type="text/javascript">
      $("#porQue").click(function() {
        $('html,body').animate({scrollTop: $("#beneficios").offset().top -50}, 1000);
      });
    </script>

  </body>

</html>
